# README #

Scripts's website: http://dodgydomain.com/?p=1

### What is this repository for? ###

Simple Python script to start playing a song on a MPD server by enabling bluetooth on a device.

Version: N/A


### How do I get set up? ###

* Summary of set up: Get the script on you local machine

* Configuration: Edit the user1, user2, userN dictionaries in the script providing your configuration.

* Dependencies: Python and following python modules: bluetooth, time, mpd

* Deployment instructions: Execute the script, enable configured in script bluetooth device and make it visible; allow some time for the device to be picked up (<1min)