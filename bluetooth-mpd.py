# -*- coding: utf-8 -*-
"""
Created on Mon Jul 21 23:51:55 2014

@author: mironq (at) o2.pl
"""

user1 = {
    'bt_addr':'18:00:2D:5D:91:42',
    'owner':'Danny',
    'song':"VH1_2001_Top_100_Albums_Of_All_Time/072_The_Jimi_Hendrix_Experience_-_Electric_Ladyland-Reissue-1968-FAF_INT/15_all_along_the_watchtower-faf.mp3",
    'title':"Jimi Hendrix - All Along the Watchtower",
    'start_second':'0',
    'welcomed':False
    }

user2 = {
    'bt_addr':'D4:93:98:C0:CA:DC',
    'owner':'Karl',
    'song':"101\ Indie\ Classics/14\ It\'s\ The\ End\ Of\ The\ World\ As\ We\ Know\ It.mp3",
    'title':"The End Of The World As We Know It",
    'start_second':'49',
    'welcomed':False
    }

user3 = {
    'bt_addr':'BC:F5:AC:84:8B:33',
    'owner':'Mirek',
    'song':"VA-Essential_Film_Themes/210-va-star_wars_episode_v_the_empire_strikes_back.flac",
    'title':"The Empire Strikes Back",
    'start_second':'10',
    'welcomed':False
    }

devices = []
devices.append(user1)
devices.append(user2)
devices.append(user3)

import bluetooth, time
from mpd import MPDClient


target_address = None

while True:
    nearby_devices = bluetooth.discover_devices()

    # informational purposes only
    for nb in nearby_devices:
        print "Found:", nb

    for btaround in nearby_devices:
        for bt in devices:

            if btaround==bt['bt_addr']:
                current_device = bt

                print "Hi", current_device['owner'], 'here is a song for you:', current_device['song']

                client = MPDClient()
                client.timeout = 10
                client.idletimeout = None
                client.connect("music.fl", 6600)
                #client.connect("localhost", 6600)

                the_song = client.playlistsearch("Title", current_device['title'])
                client.setvol(55)

                if len(the_song)==0:
                    theid=client.addid(current_device['song'])
                else:
                    theid=the_song[0]['id']

                client.seekid(theid,10)
                time.sleep(121)

                client.close()
                client.disconnect()
